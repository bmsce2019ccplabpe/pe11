#include<stdio.h>
int
main ()
{
  int num1, num2;
  printf ("Enter 2 numbers \n");
  scanf ("%d %d", &num1, &num2);
  swap (&num1, &num2);
  printf ("Number 1 = %d \nNumber 2 = %d \n", num1, num2);
}

void
swap (int *num1, int *num2)
{
  int temp;
  temp = *num1;
  *num1 = *num2;
  *num2 = temp;
}