#include <stdio.h>

int
main ()
{
  struct employee
  {
    int empid;
    char emp_name[20];
    char doj[20];
    float emp_sal;
    char address[100];
  } s1;
  printf ("enter employee id number:\n");
  scanf ("%d", &s1.empid);
  printf ("enter employee name:\n");
  scanf ("%s", s1.emp_name);
  printf ("enter date of joining\n");
  scanf ("%s", s1.doj);
  printf ("salary of employee\n");
  scanf ("%f", &s1.emp_sal);
  printf ("enter address\n");
  scanf ("%s", s1.address);
  printf ("employee details are:\n");
  printf ("id :%d\nname:%s\ndate of joining:%s\naddress:%s\nsalary%f\n",s1.empid,s1.emp_name,s1.doj,s1.address,s1.emp_sal);
  return 0;
}